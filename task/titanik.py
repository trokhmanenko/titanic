import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    def extract_title(name):
        for title in ["Mr.", "Mrs.", "Miss."]:
            if title in name:
                return title[:-1]  
        return "Other"

    df['Title'] = df['Name'].apply(extract_title)

    median_ages = df.groupby('Title')['Age'].median().round().astype(int)
    missing_values = df.groupby('Title')['Age'].apply(lambda x: x.isnull().sum()).astype(int)

    for title in ["Mr", "Mrs", "Miss"]:
        df.loc[(df['Age'].isnull()) & (df['Title'] == title), 'Age'] = median_ages[title]

    results = [
        ("Mr.", missing_values["Mr"], median_ages["Mr"]),
        ("Mrs.", missing_values["Mrs"], median_ages["Mrs"]),
        ("Miss.", missing_values["Miss"], median_ages["Miss"])
    ]

    return results
